package com.adcubum.dx;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AccountShould {

	@Mock TransactionRepository transactionRepository;
	private Account account;

	@Before
	public void initialize() {
		account = new Account(transactionRepository);
	}

	@Test
	public void
	store_a_deposit_transaction() {
		account.deposit(100);

		verify(transactionRepository).addDeposit(100);
	}



}